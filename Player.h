//
// Created by n.chernyj on 11.12.2019.
//

#ifndef BATTLESHIP_PLAYER_H
#define BATTLESHIP_PLAYER_H

#include <iostream>
#include "Logic.h"

#define ONE_DECKS 4
#define TWO_DECKS 3
#define THREE_DECKS 2
#define FOUR_DECKS 1
#define EMPTY 0
#define SHIP 1
#define NEAR_SHIP 2

class Player {
    Logic logic = Logic(10);
    int field_size;
    std::vector<std::vector<int>> field;

    void place(const int &ship_length);

    bool check_place(const int &y, const int &x, const int &square, const int &orientation, const int &deck_count);

public:
    void battle(std::ostream &output, std::istream &input);

    void arrange();

    void print_arrangement(std::ostream &output);

    Player(int size);
};

#endif //BATTLESHIP_PLAYER_H
