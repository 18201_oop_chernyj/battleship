//
// Created by n.chernyj on field_size + 1.12.2019.
//


#include "Player.h"

Player::Player(int size) {
    field_size = size;
    logic = Logic(field_size);
    field = std::vector<std::vector<int>>(field_size + 2, std::vector<int>(field_size + 2, EMPTY));
}

void Player::battle(std::ostream &output, std::istream &input) {
    std::string command;
    std::pair<int, int> coordinates;
    while (command != "Win!" && command != "Lose") {
        getline(input, command);
        if (command == "Arrange!") {
            arrange();
            print_arrangement(output);
            command.clear();
        } else if (command == "Shoot!") {
            coordinates = logic.shoot();
            output << (char) (coordinates.first + 'A') << " " << coordinates.second << std::endl;
            command.clear();
        } else if (command == "Hit") {
            logic.hit();
            coordinates = logic.shoot();
            output << (char) (coordinates.first + 'A') << " " << coordinates.second << std::endl;
            command.clear();
        } else if (command == "Kill") {
            logic.kill();
            coordinates = logic.shoot();
            output << (char) (coordinates.first + 'A') << " " << coordinates.second << std::endl;
            command.clear();
        }
    }
}

void Player::arrange() {
    srand(time(0));
    int i;
    for (i = 0; i < FOUR_DECKS; i++)
        place(4);
    for (i = 0; i < THREE_DECKS; i++)
        place(3);
    for (i = 0; i < TWO_DECKS; i++)
        place(2);
    for (i = 0; i < ONE_DECKS; i++)
        place(1);
}

void Player::print_arrangement(std::ostream &output) {
    for (int i = 1; i < field_size + 1; i++) {
        for (int j = 1; j < field_size + 1; j++)
            if (field[i][j] == SHIP)
                output << 1;
            else
                output << 0;
        output << std::endl;
    }
}

void Player::place(const int &ship_length) {
    int y = rand() % 5;
    int x = rand() % 5;
    int square = rand() % 4;
    int orientation = rand() % 2;
    // if (ship_length != 4)
    while (!check_place(y, x, square, orientation, ship_length)) {
        y = rand() % 5;
        x = rand() % 5;
        square = rand() % 4;
        orientation = rand() % 2;
    }

    int i, j;
    /*
x = (square % 2) * field_size / 2 + x;
y = square / 2 * field_size / 2 + y;
*/
    switch (square) {
        case 3: {
            switch (orientation) {
                case 0: {
                    for (i = field_size + 1 - y; i > field_size + 1 - y - 2 - ship_length; i--)
                        for (j = field_size + 1 - x; j > field_size + 1 - x - 3; j--)
                            field[i][j] = NEAR_SHIP;
                    for (i = 1; i < ship_length + 1; i++)
                        field[field_size + 1 - y - i][field_size - x] = SHIP;
                    break;
                }
                case 1: {
                    for (i = field_size + 1 - y; i > field_size + 1 - y - 3; i--)
                        for (j = field_size + 1 - x; j > field_size + 1 - x - 2 - ship_length; j--)
                            field[i][j] = NEAR_SHIP;
                    for (i = 1; i < ship_length + 1; i++)
                        field[field_size - y][field_size + 1 - x - i] = SHIP;
                    break;
                }
                default:
                    break;
            }
            break;
        }
        case 2: {
            switch (orientation) {
                case 0: {
                    for (i = field_size + 1 - y; i > field_size + 1 - y - 2 - ship_length; i--)
                        for (j = x; j < x + 3; j++)
                            field[i][j] = NEAR_SHIP;
                    for (i = 1; i < ship_length + 1; i++)
                        field[field_size + 1 - y - i][x + 1] = SHIP;
                    break;
                }
                case 1: {
                    for (i = field_size + 1 - y; i > field_size + 1 - y - 3; i--)
                        for (j = x; j < x + 2 + ship_length; j++)
                            field[i][j] = NEAR_SHIP;
                    for (i = 1; i < ship_length + 1; i++)
                        field[field_size - y][x + i] = SHIP;
                    break;
                }
                default:
                    break;
            }
            break;
        }
        case 1: {
            switch (orientation) {
                case 0: {
                    for (i = y; i < y + 2 + ship_length; i++)
                        for (j = field_size + 1 - x; j > field_size + 1 - x - 3; j--)
                            field[i][j] = NEAR_SHIP;
                    for (i = 1; i < ship_length + 1; i++)
                        field[y + i][field_size - x] = SHIP;
                    break;
                }
                case 1: {
                    for (i = y; i < y + 3; i++)
                        for (j = field_size + 1 - x; j > field_size + 1 - x - 2 - ship_length; j--)
                            field[i][j] = NEAR_SHIP;
                    for (i = 1; i < ship_length + 1; i++)
                        field[y + 1][field_size + 1 - x - i] = SHIP;
                    break;
                }
                default:
                    break;
            }
            break;
        }
        case 0: {
            switch (orientation) {
                case 0: {
                    for (i = y; i < y + 2 + ship_length; i++)
                        for (j = x; j < x + 3; j++)
                            field[i][j] = NEAR_SHIP;
                    for (i = 1; i < ship_length + 1; i++)
                        field[y + i][x + 1] = SHIP;
                    break;
                }
                case 1: {
                    for (i = y; i < y + 3; i++)
                        for (j = x; j < x + 2 + ship_length; j++)
                            field[i][j] = NEAR_SHIP;
                    for (i = 1; i < ship_length + 1; i++)
                        field[y + 1][x + i] = SHIP;
                    break;
                }
                default:
                    break;
            }
            break;
        }
        default:
            break;
    }
}

bool Player::check_place(const int &y, const int &x, const int &square, const int &orientation, const int &deck_count) {
    int i, j;
    switch (square) {
        case 3: {
            switch (orientation) {
                case 0: {
                    for (i = 1; i < deck_count + 1; i++)
                        if (field[field_size + 1 - y - i][field_size - x] == SHIP ||
                            field[field_size + 1 - y - i][field_size - x] == NEAR_SHIP)
                            return false;
                    break;
                }
                case 1: {
                    for (i = 1; i < deck_count + 1; i++)
                        if (field[field_size - y][field_size + 1 - x - i] == SHIP ||
                            field[field_size - y][field_size + 1 - x - i] == NEAR_SHIP)
                            return false;
                    break;
                }
                default:
                    break;
            }
            break;
        }
        case 2: {
            switch (orientation) {
                case 0: {
                    for (i = 1; i < deck_count + 1; i++)
                        if (field[field_size + 1 - y - i][x + 1] == SHIP ||
                            field[field_size + 1 - y - i][x + 1] == NEAR_SHIP)
                            return false;
                    break;
                }
                case 1: {
                    for (i = 1; i < deck_count + 1; i++)
                        if (field[field_size - y][x + i] == SHIP || field[field_size - y][x + i] == NEAR_SHIP)
                            return false;
                    break;
                }
                default:
                    break;
            }
            break;
        }
        case 1: {
            switch (orientation) {
                case 0: {
                    for (i = 1; i < deck_count + 1; i++)
                        if (field[y + i][field_size - x] == SHIP || field[y + i][field_size - x] == NEAR_SHIP)
                            return false;
                    break;
                }
                case 1: {
                    for (i = 1; i < deck_count + 1; i++)
                        if (field[y + 1][field_size + 1 - x - i] == SHIP ||
                            field[y + 1][field_size + 1 - x - i] == NEAR_SHIP)
                            return false;
                    break;
                }
                default:
                    break;
            }
            break;
        }
        case 0: {
            switch (orientation) {
                case 0: {
                    for (i = 1; i < deck_count + 1; i++)
                        if (field[y + i][x + 1] == SHIP || field[y + i][x + 1] == NEAR_SHIP)
                            return false;
                    break;
                }
                case 1: {
                    for (i = 1; i < deck_count + 1; i++)
                        if (field[y + 1][x + i] == SHIP || field[y + 1][x + i] == NEAR_SHIP)
                            return false;
                    break;
                }
                default:
                    break;
            }
            break;
        }
        default:
            break;
    }
    return true;
}