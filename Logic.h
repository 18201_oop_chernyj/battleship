//
// Created by n.chernyj on 11.12.2019.
//

#ifndef BATTLESHIP_LOGIC_H
#define BATTLESHIP_LOGIC_H

#include <utility>
#include <vector>

#define CELL_UNKNOWN 0
#define CELL_CHECKED 1
#define CELL_TO_HIT 2

class Logic {
    std::pair<int, int> last_random_shot, new_shot;
    std::vector<std::pair<int, int>> current_ship;
    int field_size;
    std::vector<std::vector<int>> opponent_field;
public:
    std::pair<int, int> shoot();

    void hit();

    void kill();

    void arrange();

    Logic(int field_size);
};
#endif //BATTLESHIP_LOGIC_H
