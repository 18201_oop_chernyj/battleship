//
// Created by Nikita on 1/24/2020.
//


#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include "sstream"
#include "Player.h"


TEST_CASE("Player shooting") {
    Player player(10);
    std::stringstream input, output;
    input << "Shoot!" << std::endl << "Hit" << std::endl << "Kill" << std::endl << "Miss" << std::endl << "Win!"
          << std::endl;
    player.battle(output, input);
    REQUIRE(output.str().length() >= 9);
}
