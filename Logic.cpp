//
// Created by n.chernyj on 11.12.2019.
//
#include <algorithm>
#include <iostream>
#include "Logic.h"

Logic::Logic(int size) {
    field_size = size;
    opponent_field = std::vector<std::vector<int>>(field_size, std::vector<int>(field_size, CELL_UNKNOWN));
}

std::pair<int, int> Logic::shoot() {
    int i, j;
    for (i = 0; i < field_size; i++) {
        for (j = 0; j < field_size; j++) {
            if (opponent_field[i][j] == CELL_TO_HIT) {
                opponent_field[i][j] = CELL_CHECKED;
                new_shot = std::make_pair(i, j);
                return new_shot;
            }
        }
    }
    while (opponent_field[last_random_shot.first][last_random_shot.second] == CELL_CHECKED) {
        last_random_shot.second = (last_random_shot.second + 1) % field_size;
        if (last_random_shot.second == 0) {
            last_random_shot.first = (last_random_shot.first + 3) % field_size;
            if (last_random_shot.first == 0)
                last_random_shot.first++;
        } else
            last_random_shot.first = (last_random_shot.first + 1) % field_size;
        //std::cout << (char)(last_random_shot.first + 'A') << " " << last_random_shot.second;
    }
    opponent_field[last_random_shot.first][last_random_shot.second] = CELL_CHECKED;
    new_shot = last_random_shot;
    return new_shot;
}

void Logic::hit() {
    current_ship.push_back(new_shot);

    if (new_shot.first > 0) {
        if (new_shot.second > 0)
            opponent_field[new_shot.first - 1][new_shot.second - 1] = CELL_CHECKED;
        if (new_shot.second < field_size - 1)
            opponent_field[new_shot.first - 1][new_shot.second + 1] = CELL_CHECKED;
    }
    if (new_shot.first < field_size - 1) {
        if (new_shot.second > 0)
            opponent_field[new_shot.first + 1][new_shot.second - 1] = CELL_CHECKED;
        if (new_shot.second < field_size - 1)
            opponent_field[new_shot.first + 1][new_shot.second + 1] = CELL_CHECKED;
    }

    if (new_shot.first > 0 && (current_ship.size() == 1 || std::find(current_ship.begin(), current_ship.end(),
                                                                     std::make_pair(new_shot.first + 1,
                                                                                    new_shot.second)) !=
                                                           current_ship.end()))
        if (opponent_field[new_shot.first - 1][new_shot.second] != CELL_CHECKED)
            opponent_field[new_shot.first - 1][new_shot.second] = CELL_TO_HIT;
    if (new_shot.first < field_size - 1 && (current_ship.size() == 1 ||
                                            std::find(current_ship.begin(), current_ship.end(),
                                                      std::make_pair(new_shot.first - 1, new_shot.second)) !=
                                            current_ship.end()))
        if (opponent_field[new_shot.first + 1][new_shot.second] != CELL_CHECKED)
            opponent_field[new_shot.first + 1][new_shot.second] = CELL_TO_HIT;
    if (new_shot.second > 0 && (current_ship.size() == 1 || std::find(current_ship.begin(), current_ship.end(),
                                                                      std::make_pair(new_shot.first,
                                                                                     new_shot.second + 1)) !=
                                                            current_ship.end()))
        if (opponent_field[new_shot.first][new_shot.second - 1] != CELL_CHECKED)
            opponent_field[new_shot.first][new_shot.second - 1] = CELL_TO_HIT;
    if (new_shot.second < field_size - 1 && (current_ship.size() == 1 ||
                                             std::find(current_ship.begin(), current_ship.end(),
                                                       std::make_pair(new_shot.first, new_shot.second - 1)) !=
                                             current_ship.end()))
        if (opponent_field[new_shot.first][new_shot.second + 1] != CELL_CHECKED)
            opponent_field[new_shot.first][new_shot.second + 1] = CELL_TO_HIT;
}

void Logic::kill() {
    current_ship.push_back(new_shot);

    if (new_shot.first > 0) {
        if (new_shot.second > 0)
            opponent_field[new_shot.first - 1][new_shot.second - 1] = CELL_CHECKED;
        if (new_shot.second < field_size - 1)
            opponent_field[new_shot.first - 1][new_shot.second + 1] = CELL_CHECKED;
    }
    if (new_shot.first < field_size - 1) {
        if (new_shot.second > 0)
            opponent_field[new_shot.first + 1][new_shot.second - 1] = CELL_CHECKED;
        if (new_shot.second < field_size - 1)
            opponent_field[new_shot.first + 1][new_shot.second + 1] = CELL_CHECKED;
    }
    std::sort(current_ship.begin(), current_ship.end());

    if (current_ship.front().first > 0)
        opponent_field[current_ship.front().first - 1][current_ship.front().second] = CELL_CHECKED;
    if (current_ship.front().first < field_size - 1)
        opponent_field[current_ship.front().first + 1][current_ship.front().second] = CELL_CHECKED;
    if (current_ship.front().second > 0)
        opponent_field[current_ship.front().first][current_ship.front().second - 1] = CELL_CHECKED;
    if (current_ship.front().second < field_size - 1)
        opponent_field[current_ship.front().first][current_ship.front().second + 1] = CELL_CHECKED;

    if (current_ship.back().first > 0)
        opponent_field[current_ship.back().first - 1][current_ship.back().second] = CELL_CHECKED;
    if (current_ship.back().first < field_size - 1)
        opponent_field[current_ship.back().first + 1][current_ship.back().second] = CELL_CHECKED;
    if (current_ship.back().second > 0)
        opponent_field[current_ship.back().first][current_ship.back().second - 1] = CELL_CHECKED;
    if (current_ship.back().second < field_size - 1)
        opponent_field[current_ship.back().first][current_ship.back().second + 1] = CELL_CHECKED;

    current_ship.clear();
}


