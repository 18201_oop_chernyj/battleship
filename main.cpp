//
// Created by n.chernyj on 11.12.2019.
//

#include <string>
#include <iostream>
#include "Player.h"

#define FIELD_SIZE 10

int main() {
    Player player(FIELD_SIZE);
    //player.arrange();
    //player.print_arrangement(std::cout);
    player.battle(std::cout, std::cin);
    return 0;
}